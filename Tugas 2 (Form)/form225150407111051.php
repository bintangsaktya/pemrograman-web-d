<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>FORM</h1>
    <form action="" method="GET">
        <label for="">NIM : </label>
        <input type="text" id="nim" name="nim"> <br>

        <label for="">Nama : </label>
        <input type="text" id="nim" name="nama"> <br>


        <label for="">Password : </label>
        <input type="password" name="password" id="password"> <br><br>

        <label for="">Program Studi : </label><br>
        <input type="radio" id="sistemInformasi" name="programStudi" value="Sistem Informasi"> Sistem Informasi<br>
        <input type="radio" id="teknikInformatika" name="programStudi" value="Teknik Informatika"> Teknik Informatika<br>
        <input type="radio" id="teknologiInformasi" name="programStudi" value="Teknologi Informasi"> Teknologi Informasi<br>
        <input type="radio" id="teknikKomputer" name="programStudi" value="Teknik Komputer"> Teknik Komputer<br>
        <input type="radio" id="pendidikanTeknologiInformasi" name="programStudi" value="Pendidikan Teknologi Informasi"> Pendidikan Teknologi Informasi<br>
        
        <br>

        <label for="mataKuliah">Pilih Mata Kuliah:</label>
        <select id="mataKuliah" name="mataKuliah">
            <option value="Pemrograman Dasar">Pemrograman Dasar</option>
            <option value="Pemrograman Lanjut">Pemrograman Lanjut</option>
            <option value="Dasar Basis Data">Dasar Basis Data</option>
            <option value="Pemrograman Web">Pemrograman Web</option>
            <option value="Pemrograman Web Lanjut">Pemrograman Web Lanjut</option>
        </select>

        

        <br>
        <input type="submit">

    </form>

    <?php
    if (isset($_GET['nama'])) { 
        $nim = isset($_GET["nim"]) ? $_GET["nim"] : "Tidak diisi";
        $nama = isset($_GET["nama"]) ? $_GET["nama"] : "Tidak diisi";
        $password = isset($_GET["password"]) ? $_GET["password"] : "Tidak diisi";
        $programStudi = isset($_GET["programStudi"]) ? $_GET["programStudi"] : "Tidak diisi";
        $mataKuliah = isset($_GET["mataKuliah"]) ? $_GET["mataKuliah"] : "Tidak diisi";
        
        echo "<h2>INFORMASI MAHASISWA</h2>";
        echo "<table border='1'>";
        echo "<tr><td>NIM</td><td>$nim</td></tr>";
        echo "<tr><td>Nama</td><td>$nama</td></tr>";
        echo "<tr><td>Password</td><td>$password</td></tr>";
        echo "<tr><td>Program Studi</td><td>$programStudi</td></tr>";
        echo "<tr><td>Mata Kuliah</td><td>$mataKuliah</td></tr>";
        echo "</table>";
    }
    ?>


</body>
</html>